import operations_interv as op
import numpy as np

def init_im(n, interv):
    '''Initialisation d_(n+1), 
    prend en entrée n et le centre réel des intervalles'''
    res = np.zeros(n, dtype=[('real', np.float64), ('imaginary', np.float64), ('vect', np.float64), ('err_radius', np.float64)])
    res['vect'] = np.array(interv)['center']
    return res

def init_b(n, interv):
    '''Inialisation des intervalles (0, erreur(I)),
    prend en argument n=len(I) et I'''
    res = np.copy(interv)
    res = np.array(res)
    res['center'] = np.zeros(n)
    return res.view(op.indarray)

def im_from_interv(n, interv):
    '''Transforme des intervalles réels en intervalles complexes,
    prend en arguments n=len(I) et I'''
    res = np.zeros(n, dtype=[('real', np.float64), ('imaginary', np.float64), ('vect', np.float64), ('err_radius', np.float64)])
    i=np.array(interv)
    res['real'] = i['center']
    res['err_radius'] = i['radius']
    return res

def im_proj(n, d, u, p):
    '''Renvoie la projection sur les réels 
    suivant le complexe associé à l'intervalle,
    prend en arguments len(I), I ; et u et p pour le calcul de l'erreur'''
    res = np.zeros(n, dtype=[('center', np.float64), ('radius', np.float64)])
    res['center'] = d['real']
    res['radius'] = op.proj_err(d['err_radius'], p, u['radius'])
    return res.view(op.indarray)

def box(f, interv, prem_type=True, epsilon=0.):
    '''box(f, I, prem_type=True, e=0) prend en arguments
    - f, une série de Tchebychev de premier type si prem_type, deuxieme type sinon
    - I un tableau d'intervalle, - e l'erreur
    et renvoie box(f)(I) + [-e, e]'''
    n = len(interv)
    d_k = init_im(n, interv)
    b = init_b(n, interv)
    p = op.proj(d_k['vect'])
    u_k1 = op.sarray(np.zeros(n))
    u_k = op.sarray(np.zeros(n))
    if len(f) == 0 :
        return u_k + op.iarray([(0, epsilon)])
    for a_k in f[:0:-1] :
        s = np.array([a_k]).view(op.indarray) + 2*b*u_k
        d_k = op.add_im(op.rotat(d_k), im_from_interv(n, s))
        #recurrence de l'algorithme de Clenshaw
        u_k1 = np.copy(u_k)
        u_k = im_proj(n, d_k, np.array(u_k1), p)
    if prem_type :
        return f[0:1:] + interv*u_k - u_k1 + op.iarray([(0, epsilon)])
    else :
        return f[0:1:] + 2*interv*u_k - u_k1 + op.iarray([(0, epsilon)])

def box_inter_0(f, interv, prem_type=True, epsilon=0.):
    ''' box_inter_0(f, interv, prem_type=True, e=0)
    renvoie pour chaque intervalle I si
    box(f, I, prem_type, epsilon) contient 0'''
    n=len(interv)
    box_f = np.array(box(f, interv, prem_type, epsilon))
    def contient_zero(c, r) :
        return (abs(c) <= r)
    return contient_zero(box_f['center'], box_f['radius'])

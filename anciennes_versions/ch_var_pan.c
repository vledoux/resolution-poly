#include "stdio.h"
#include "arb.h"
#include "arb_poly.h"
#include "time.h"

//test

int prec = 3200;
slong taille = 3201;

void reverse(arb_poly_t res, slong n)
{
    arb_t coef_i, coef_n_i;
    arb_init(coef_i); arb_init(coef_n_i);
    for (int i=0; i<n/2; i++) {
        arb_poly_get_coeff_arb(coef_i, res, i);
        arb_poly_get_coeff_arb(coef_n_i, res, n-i-1);
        arb_poly_set_coeff_arb(res, i, coef_n_i);
        arb_poly_set_coeff_arb(res, n-i-1, coef_i);
    }
    arb_clear(coef_i); arb_clear(coef_n_i);
    return;
}

void carre(arb_poly_t res, slong n)
{
    arb_poly_fit_length(res, 2*n);
    arb_t coef_i; arb_init(coef_i);
    for (int i=n-1; i>=0; i--) {
        arb_poly_get_coeff_arb(coef_i, res, i);
        arb_poly_set_coeff_arb(res, 2*i, coef_i);
        arb_zero(coef_i);
        arb_poly_set_coeff_arb(res, 2*i+1, coef_i);
    }
    arb_clear(coef_i);
    return;
}

void affine(arb_poly_t res, slong a, slong b)
{
    arb_poly_t un, x;
    arb_poly_init(un); arb_poly_init(x);
    arb_poly_one(un); arb_poly_shift_left(x, un, 1);
    
    arb_t coef_a, coef_b;
    arb_init(coef_a); arb_init(coef_b);
    arb_set_si(coef_a, a); arb_set_si(coef_b, b);
    
    arb_poly_scalar_mul(un, un, coef_b, prec);
    arb_poly_scalar_mul(x, x, coef_a, prec);
    arb_poly_add(res, x, un, prec);
    
    arb_clear(coef_a); arb_clear(coef_b);
    arb_poly_clear(un); arb_poly_clear(x);
    return;
}

void compose(arb_ptr res, slong n, arb_t a, arb_t c)
{
    // On suppose poly2 de degré 1
    _arb_poly_taylor_shift_convolution(res, c, n, prec);

    // Le code ci-dessous est copié de la librairie Arb,
    // qui est sous licence GNU LGPL
    if (!arb_is_one(a)) {
        if (arb_equal_si(a, -1)) {
            for (slong i = 1; i < n; i += 2)
                arb_neg(res + i, res + i);
        }
        else {
            arb_t t;
            arb_init(t);
            arb_set(t, a);

            for (slong i = 1; i < n; i++) {
                arb_mul(res + i, res + i, t, prec);
                if (i + 1 < n)
                    arb_mul(t, t, a, prec);
            }

            arb_clear(t);
        }
    }
    
    return;
}

int main()
{
    long clk_tck = CLOCKS_PER_SEC;
    clock_t t1, t2;
    t1 = clock();
    
    arb_t coef; arb_init(coef);
    arb_poly_t x; arb_poly_init(x);
    arb_poly_fit_length(x, taille);
    arb_set_d(coef, 1); arb_poly_set_coeff_arb(x, taille-1, coef);
    arb_poly_t af; arb_poly_init(af);
    
    affine(af, 2, -1);
    //arb_poly_compose(x, x, af, prec);
    compose(x->coeffs, taille, af->coeffs+1, af->coeffs);
    
    reverse(x, taille);
    affine(af, -1, 1);
    //arb_poly_compose(x, x, af, prec);
    compose(x->coeffs, taille, af->coeffs+1, af->coeffs);

    carre(x, taille);
    affine(af, 2, -1);
    //arb_poly_compose(x, x, af, prec);
    compose(x->coeffs, 2*taille-1, af->coeffs+1, af->coeffs);

    reverse(x, 2*taille-1);
    affine(af, 1, 1);
    //arb_poly_compose(x, x, af, prec);
    compose(x->coeffs, 2*taille-1, af->coeffs+1, af->coeffs);

    arb_poly_scalar_mul_2exp_si(x, x, 3-2*taille);
    
    arb_poly_shift_right(x, x, taille-1);
    arb_poly_get_coeff_arb(coef, x, 0);
    arb_mul_2exp_si(coef, coef, -1);
    arb_poly_set_coeff_arb(x, 0, coef);
    
    arb_printd(x->coeffs, 50); printf("\n");
    //arb_poly_printd(x, 50); printf("\n");
    
    arb_clear(coef);
    arb_poly_clear(af);
    arb_poly_clear(x);
    
    t2 = clock();
    printf("Temps (s) : %lf \n",
           (double)(t2-t1)/(double)clk_tck);
    return 0;
}

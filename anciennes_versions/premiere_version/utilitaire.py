import numpy as np
import np_intervals as npi
import chebyshev as ch
import box as box

def conversion(P):
    ''' prend en entrée une fonction polynomiale P,
        i.e. ses coefficients dans la base canonique,
        et renvoie
        ((le polynôme dans la base de Tchebychev,
        l'erreur induite sur le polynôme tronqué),
        (idem pour le polynôme dérivé))
        '''
    degre=len(P)
    P_cheb = ch.to_chebyshev(P)
    P_derivee = np.fromfunction(lambda i:(i+1)*P[i+1], (degre-1,), dtype=int)
    P_cheb_d = ch.to_chebyshev(P_derivee)
    
    def cumul_err(t, d):
        err = np.zeros(d+1)
        for i in range(d-1,-1,-1):
            err[i] = abs(t[i])+err[i+1]
        return err
    P_err, P_derr = cumul_err(P_cheb,degre), cumul_err(P_cheb_d,degre-1)
    return ((P_cheb, P_err), (P_cheb_d, P_derr))

def calcul_deg_err(err, epsi):
    ''' calcul_deg_err(err, epsi)
        prend en entrée :
        - err, un tableau tel que
        err[i] = erreur entre le polynôme tronqué de degré i
        et le polynôme de départ ;
        - epsi, l'erreur souhaitée ;
        et renvoie le degré où tronquer le polynôme '''
    n,m=0,len(err)-1
    while n != m :
        mid = (n+m)//2
        if err[mid] > epsi :
            n = mid+1
        else :
            m = mid
    return m

def filtre_sol(P, sol):
    ''' filtre_sol prend en entrée :
        - un polynôme P dans la base canonique ;
        - une liste d'intervalles sol
        où P est monotone ;
        et renvoie pour chaque intervalle
        s'il contient une racine de P '''
    degre = len(P)
    
    def resol(x):
        y=0
        for i in P[::-1]:
            y=y*x+i
        return y

    Y_1 = resol(-np.array(sol)['neglow'])
    Y_2 = resol(np.array(sol)['up'])
    return (Y_1*Y_2 <= 0)


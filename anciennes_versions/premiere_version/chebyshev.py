import np_intervals as npi
import numpy as np

def to_chebyshev(P) :
    ''' renvoie, pour un polynome P donne sous forme de coefficients de monomes
        P(X) = P[0] + P[1]*X + ... + P[deg(P)]*X^(deg(P)),
        les coefficients c_j de Chebyshev tels que
        P(X) = c_0 + c_1*T_1[X] + ... + c_k*T_k[X]
        '''
    n = len(P) # n >= deg(P)+1
    if n <= 1 :
        return P
    
    x_k = np.zeros(n)
    x_k[0] = 1
    cheb = np.zeros(n)
    for i in range(n) :
        # x_k contient les coefficients a_j tels que
        # x^k = a_0 + a_1*T_1(x) + ... + a_(n-1)*T_(n-1)(x)
        cheb += P[i]*x_k
        #err += abs(P[i])*x_k
        # calcul de x_(k+1)
        pb, x_k[0] = x_k[0], 0 #pb de condition limite
        x_k = 0.5*(np.roll(x_k, 1) + np.roll(x_k, -1))
        x_k[1] += pb
    return cheb

import np_intervals as npi
import numpy as np
import box as box
import utilitaire as util
import split_npi as s
import time as time

def extraire_racines(P, f, mode_test) :
    ''' prend en argument
        - un polynôme P, donné par ses coefficients dans la base canonique ;
        - une fonction f qui contrôle l'erreur de troncation ;
        - mode_test (booléen) pour les test ;
        et renvoie les racines dans [-1, 1] de P ainsi que différentes statistiques :
        (sol[filtre], (max_tour, max_tronque, max_tronque_derivee, nb_interv))
        '''
    P=np.array(P)
    ((P_cheb, P_cheb_err), (P_cheb_derivee, P_cheb_derivee_err)) = util.conversion(P)
    sol = (np.array([], dtype=[('neglow',np.float64), ('up',np.float64)])).view(type=npi.indarray)
    a_traiter = npi.iarray([(-1,1)])

    tour=1
    max_tour, max_tronque, max_tronque_derivee = 0, 0, 0
    max_dispersion_interv = 0
    nb_interv = 0
    while np.size(a_traiter) != 0 :
        if tour > max_tour :
            max_tour = tour
        nb_interv += len(a_traiter)
        epsilon, tour = f(tour), tour+1
        degre_err = util.calcul_deg_err(P_cheb_err, abs(epsilon))
        degre_err_derivee = util.calcul_deg_err(P_cheb_derivee_err, abs(epsilon))
    
        if mode_test :
            i = np.array(a_traiter)
            mid = 0.5*(i['up'] - i['neglow'] )
            moy = np.sum(np.abs(mid))/len(i)
            print("moyenne des centres d'intervalles : ", moy)
    
        (image_P, disp_P) = box.box_inter_0(P_cheb[:degre_err], a_traiter, abs(epsilon))
        a_traiter = a_traiter[image_P]
        (image_derivee_P, disp_derivee) = box.box_inter_0(P_cheb_derivee[:degre_err_derivee], a_traiter, abs(epsilon))
        sol_incr = a_traiter[np.logical_not(image_derivee_P)]
        sol = np.concatenate((sol, sol_incr)).view(type=npi.indarray)
        a_traiter = a_traiter[image_derivee_P]
        
        if mode_test :
            print("polynôme : ", degre_err, " ", disp_P*pow(2,tour)/8)
            # taille d'un intervalle : 8/pow(2, tour)
            print("dérivée : ", degre_err_derivee, " ", disp_derivee*pow(2,tour)/8)
    
        a_traiter = s.split(a_traiter)
        if degre_err > max_tronque :
            max_tronque = degre_err
        if degre_err_derivee > max_tronque_derivee :
            max_tronque_derivee = degre_err_derivee
    
    filtre = util.filtre_sol(P, sol)
    return (sol[filtre], (max_tour, max_tronque, max_tronque_derivee, nb_interv))

def extraire_racines_simpl(P) :
    '''prend en argument une fonction polynomiale P
        donnée par ses coefficients dans la base canonique,
        et renvoie, en utilisant Horner et un algo de subdivision,
        (nb_iterations_algo, racines dans [-1, 1])'''
    P=np.array(P)
    P_derivee=np.fromfunction(lambda i:(i+1)*P[i+1], (len(P)-1,), dtype=int)
    sol = (np.array([], dtype=[('neglow',np.float64), ('up',np.float64)])).view(type=npi.indarray)
    a_traiter = npi.iarray([(-1,1)])
    
    tour=1
    while np.size(a_traiter) != 0 :
        tour += 1
        image_P = box.box_simpl(P, a_traiter)
        a_traiter = a_traiter[image_P]
        image_derivee_P = box.box_simpl(P_derivee, a_traiter)
        sol_incr = a_traiter[np.logical_not(image_derivee_P)]
        sol = np.concatenate((sol, sol_incr)).view(type=npi.indarray)
        a_traiter = a_traiter[image_derivee_P]
        a_traiter = s.split(a_traiter)

    filtre = util.filtre_sol(P, sol)
    return (tour, sol[filtre])

def main(f, x, mode_test=False) :
    ''' main(f, x, mode_test=False) prend en argument :
        - une fonction f qui détermine les approximations
        qui seront faites lorsqu'on tronquera le polynôme ;
        - le degré x du polynôme qui sera généré
        et dont on va chercher les racines :
        - mode_test ne sert que pour les tests et peut étre omis ;
        et retourne
        (racines dans [-1, 1],
        nb_iterations, degre_tronque,
        degre_tronque_derivee, nb_interv_consideres) '''
    P = np.random.rand(x)
    t = time.time()
    (racines, (nb_iterations, degre_tronque, degre_tronque_derivee, nb_interv)) = extraire_racines(P,f, mode_test)
    t = time.time() - t
    # print("Nb de racines dans [-1, 1] : ", len(racines))
    # print("Nb d'iterations de l'algorithme : ", nb_iterations)
    # print("Plus grand degré de troncation : ", degre_tronque)
    # print("Plus grand degré de troncation de la derivee : ", degre_tronque_derivee)
    # print("Nb intervalles considérés : ", nb_interv)
    # print("Temps d'exécution : ", t)
    return (racines, nb_iterations, degre_tronque, degre_tronque_derivee, nb_interv)


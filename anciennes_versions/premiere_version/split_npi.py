import numpy as np
import np_intervals as npi

def mid_bas(interv):
    ''' pour chaque intervalle [a, b], renvoie [a, (a+b)/2]
        attention, renvoie un array numpy '''
    interv=np.array(interv)
    interv['up'] = 0.5*(interv['up']-interv['neglow'])
    return interv

def mid_haut(interv):
    ''' pour chaque intervalle [a, b], renvoie [(a+b)/2, b]
        attention, renvoie un array numpy '''
    interv=np.array(interv)
    interv['neglow'] = 0.5*(interv['neglow']-interv['up'])
    return interv

def split(interv):
    ''' pour une liste d'intervalles [a,b]::...,
        renvoie [a, (a+b)/2]::...::[(a+b)/2, b]::... '''
    return np.concatenate((mid_bas(interv), mid_haut(interv))).view(type=npi.indarray)

import np_intervals as npi
import numpy as np

def box(f, interv, epsilon=0.):
    """ box(f, interv, epsilon) prend en arguments
    - une fonction polynomiale f sont forme [a_0, ...,a_n],
    la suite des coefficients de Chebyshev tels que
    f(x) = a_0 + a_1*T_1(x) + ... ;
    - les intervalles interv dont on cherche l'image ;
    - l'approximation epsilon qui a ete faite sur f ;
    et renvoie box(f)(interv) + [-epsilon, epsilon] """
    n = len(interv)
    b_k2 = npi.iarray(np.zeros((n,2)))
    b_k1 = npi.iarray(np.zeros((n,2)))
    b_k = npi.iarray(np.zeros((n,2)))
    if len(f) == 0 :
        return b_k + npi.iarray([[-epsilon, epsilon]])
    for a_k in f[:0:-1] :
        b_k2, b_k1 = b_k1, b_k
        #recurrence de l'algorithme de Clenshaw
        b_k = a_k + 2*interv*b_k1 - b_k2
    return f[0] + interv*b_k - b_k1 + npi.iarray([[-epsilon, epsilon]])

def box_inter_0(f, interv, epsilon=0.):
    """ box_inter_0(f, interv, epsilon) prend en arguments
        - une fonction polynomiale f sont forme [a_0, ...,a_n],
        la suite des coefficients de Chebyshev tels que
        f(x) = a_0 + a_1*T_1(x) + ... ;
        - les intervalles interv dont on cherche l'image ;
        - l'approximation epsilon qui a ete faite sur f ;
        et renvoie si box(f)(interv) + [-epsilon, epsilon] contient 0,
        ainsi que la plus grande largeur des intervalles box(f)(interv)"""
    n=len(interv)
    box_f = np.array(box(f, interv, epsilon)) # box_f = box(f)(interv) + [-epsilon, epsilon]
    def contient_zero(x1, x2) :
        return x1*x2 >= 0
    return (contient_zero(box_f['neglow'], box_f['up']), np.max(np.concatenate((np.abs(box_f['neglow'] + box_f['up']), [0]))))

def box_simpl(f, interv):
    ''' prend en argument :
        - une fonction polynomiale f,
        i.e. ses coefficients dans la base canonique;
        - la liste des intervalles interv dont on recherche l'image;
        et renvoie, via l'algorithme de Horner en arithmétique d'intervalle,
        si box(f)(interv) contient 0 '''
    n=len(interv)
    y = npi.iarray(np.zeros((n,2)))
    for a_k in f[::-1] :
        y = a_k + y*interv
    y = np.array(y)
    def contient_zero(x1, x2) :
        return x1*x2 >= 0
    return contient_zero(y['neglow'],y['up'])

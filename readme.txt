Nécessite :
- Python (de préférence version 3+)
- bibliothèque Numpy de Python
- Arb, ainsi que GMP, MPFR et Flint dont elle dépend

Aller dans le dossier operations_interv
(pas operations_interv/operations_interv),
entrer :
CC=gcc-9 python3 setup.py install
pour installer le module operations_interv

Aller dans le dossier resolution_poly
le solveur est dans fichier resol.py

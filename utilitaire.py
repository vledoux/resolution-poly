import numpy as np
import operations_interv as op
import chebyshev as ch
import box as box

def conversion(P, base_canonique=True):
    '''conversion de P vers la base de Tchebychev
    et calcul de la dérivée et des erreurs associées'''
    degre = len(P)
    if base_canonique:
        P_cheb = ch.to_chebyshev(P)
    else:
        P_cheb = P
    facteur = np.fromfunction(lambda i:(i+1), (degre-1,),dtype=int)
    P_derivee = op.sarray(facteur)*(np.roll(P,-1)[:degre-1:])
    if base_canonique:
        P_cheb_d = ch.to_chebyshev(P_derivee)
    else:
        P_cheb_d = P_derivee

    def cumul_err(t,d):
        err = np.zeros(d+1)
        err[:d:] = op.err_cumul(t)
        return err
    def cumul_err_u(t,d):
        err = np.zeros(d+1)
        err[:d:] = op.err_cumul_u(t)
        return err
    P_err = cumul_err(P_cheb,len(P_cheb))
    if base_canonique:
        P_derr = cumul_err(P_cheb_d,len(P_cheb_d))
    else:
        P_derr = cumul_err_u(P_cheb_d,len(P_cheb_d))
    return ((P_cheb, P_err), (P_cheb_d, P_derr))

def calcul_deg_err(err, epsi):
    '''calcul pour une erreur epsi, à quel degré peut être tronqué le polynome'''
    n,m=0,len(err)-1
    while n != m :
        mid = (n+m)//2
        if err[mid] > epsi :
            n = mid+1
        else :
            m = mid
    return m

def filtre_sol(P, sol, base_canonique=True):
    '''pour des intervalles sol sur lequel P est monotone,
    renvoie si sol contient une racine
    en évaluant P à chaque extrémité de l'intervalle'''
    degre = len(P)
    n = len(sol)
    
    X_1 = op.inf(sol)
    X_2 = op.sup(sol)

    if base_canonique:
        Y_1, Y_2 = op.sarray(np.zeros(n)), op.sarray(np.zeros(n))
        for i in P[::-1]:
            Y_1 = Y_1*X_1 + np.array([i]).view(op.indarray)
            Y_2 = Y_2*X_2 + np.array([i]).view(op.indarray)
        return op.tri(np.array(Y_1), np.array(Y_2))

    else:
        Y_1, Y_2 = box.box(P, X_1), box.box(P, X_2)
        return op.tri(np.array(Y_1), np.array(Y_2))



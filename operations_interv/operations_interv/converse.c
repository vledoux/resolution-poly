#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include "Python.h"
#include "math.h"
#include "float.h"
#include "fenv.h"
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

#include "stdio.h"
#include "arb.h"
#include "arb_poly.h"
#include "time.h"

/*
    DEFINITION DES CONSTANTES
    sqrt(2), 1/sqrt(2)
    cos(pi/8), cos(3pi/8), cos(9pi/8)
    polynome (1+X)/2
*/

slong precision=53;

arb_t racine_2, inv_racine_2;
arb_t coef_dct4_1, coef_dct4_3, coef_dct4_9;
arb_poly_t transfo;

void init(void){
    arb_init(racine_2); arb_init(inv_racine_2);
    arb_init(coef_dct4_1); arb_init(coef_dct4_3); arb_init(coef_dct4_9);
    arb_sqrt_ui(racine_2, 2, precision);
    arb_div_ui(inv_racine_2, racine_2, 2, precision);
    
    arb_set_ui(coef_dct4_1, 1);
    arb_div_ui(coef_dct4_1, coef_dct4_1, 8, precision);
    arb_cos_pi(coef_dct4_1, coef_dct4_1, precision);
    
    arb_set_ui(coef_dct4_3, 3);
    arb_div_ui(coef_dct4_3, coef_dct4_3, 8, precision);
    arb_cos_pi(coef_dct4_3, coef_dct4_3, precision);
    
    arb_set_ui(coef_dct4_9, 9);
    arb_div_ui(coef_dct4_9, coef_dct4_9, 8, precision);
    arb_cos_pi(coef_dct4_9, coef_dct4_9, precision);

    arb_poly_init(transfo);
    arb_poly_set_coeff_si(transfo, 0, 1);
    arb_poly_set_coeff_si(transfo, 1, 1);
    arb_poly_scalar_mul_2exp_si(transfo, transfo, -1);
}

void finit(void){
    arb_clear(inv_racine_2);
    arb_clear(racine_2);
    arb_clear(coef_dct4_1); arb_clear(coef_dct4_3); arb_clear(coef_dct4_9);
    arb_poly_clear(transfo);
}

/*
 Le lecteur malchanceux pourra constater que
 j'ai découvert l'opérateur de décalage de bits,
 qui est beaucoup trop cool mais produit un code assez abscons
*/

/* 
    DEFINITION DES MATRICES
    J, -I, sqrt(2)I, D
    P, T0, T1, A    
*/

void J (arb_ptr poly, slong n) {
    arb_t temp; arb_init(temp);
    for(slong i=0; i<(n>>1); i+=1){
        arb_set(temp, poly+i);
        arb_set(poly+i, poly+(n-i-1));
        arb_set(poly+(n-i-1), temp);
    }
    arb_clear(temp);
    return;
}

void Ineg (arb_ptr poly, slong n) {
    for(slong i=1; i<n; i+=2)
        arb_neg(poly+i, poly+i);
    return;
}

void mul_racine_2 (arb_ptr poly, slong n) {
    for(slong i=0; i<n; i+=1) {
        arb_mul(poly+i, poly+i, racine_2, precision);
    }
}

/* Les fonctions suivantes n'ont de sens que pour n pair */

void D (arb_ptr poly, slong n) {
    for(slong i=1; i<n; i+=2)
        arb_neg(poly+i, poly+i);
    return;
}

void P (arb_ptr poly, slong n) {
    arb_poly_t temp; arb_poly_init(temp);
    arb_poly_fit_length(temp, n);
    for(slong i=(n>>1); i<n; i+=1)
        arb_set(temp->coeffs+i, poly+i);
    for(slong i=(n>>1)-1; i>=0; i-=1)
        arb_set(poly+(i<<1), poly+i);
    for(slong i=0; i<(n>>1);i+=1)
        arb_set(poly+(i<<1)+1, temp->coeffs+(n>>1)+i);
    arb_poly_clear(temp);
    return;
}

void T0 (arb_ptr poly, slong n) {
    arb_poly_t temp; arb_poly_init(temp);
    arb_poly_fit_length(temp, n);
    /* Je ne sais pas pourquoi la ligne suivante est nécessaire,
    mais il y a une segmentation fault si je l'enlève */
    _arb_poly_set_length(temp, n);
    arb_t coef; arb_init(coef);
    for(slong i=0; i<(n>>1); i+=1) {
        arb_add(coef, poly+i, poly+n-i-1, precision);
        arb_set(temp->coeffs+i, coef);
    }
    for(slong i=0; i<(n>>1); i+=1) {
        arb_sub(coef, poly+i, poly+n-i-1, precision);
        arb_set(temp->coeffs+(n>>1)+i, coef);
    }
    for(slong i=0; i<n; i+=1) {
        arb_set(poly+i, temp->coeffs+i);
    }
    arb_poly_clear(temp); arb_clear(coef);
    return;
}

void T1 (arb_ptr poly, slong n) {
    arb_t coef; arb_init(coef);
    arb_t coef_sin, coef_cos; arb_init(coef_sin); arb_init(coef_cos);
    arb_t x_i, x_ni; arb_init(x_i); arb_init(x_ni);
    
    for(slong i=0; i<n>>1; i+=1) {
        arb_set_si(coef, (i<<1)+1);
        arb_div_si(coef, coef, n<<2, precision);
        arb_sin_cos_pi(coef_sin, coef_cos, coef, precision);
        
        arb_mul(x_i, coef_cos, poly+i, precision);
        arb_mul(coef, coef_sin, poly+n-i-1, precision);
        arb_add(x_i, x_i, coef, precision);
        
        arb_mul(x_ni, coef_cos, poly+n-i-1, precision);
        arb_mul(coef, coef_sin, poly+i, precision);
        arb_sub(x_ni, x_ni, coef, precision);
        if(!(((n>>1)-i)&1 /* c'est un modulo 2, n'est-ce pas merveilleux ? */)) {
            arb_neg(x_ni, x_ni);
        }
        
        arb_set(poly+i, x_i);
        arb_set(poly+n-i-1, x_ni);
    }
    
    arb_clear(coef_sin); arb_clear(coef_cos);
    arb_clear(x_i); arb_clear(x_ni); arb_clear(coef);
    return;
}

void A (arb_ptr poly, slong n) {
    arb_poly_t temp; arb_poly_init(temp);
    arb_poly_fit_length(temp, n);
    _arb_poly_set_length(temp, n);

    J(poly+(n>>1), n>>1);
    D(poly+(n>>1), n>>1);

    arb_set(temp->coeffs, poly);
    arb_neg(temp->coeffs+n-1, poly+n-1);
    
    for(slong i=1; i<n>>1; i+=1){
        arb_add(temp->coeffs+i, poly+i, poly+(n>>1)+i-1, precision);
        arb_mul(temp->coeffs+i, temp->coeffs+i, inv_racine_2, precision);
        arb_sub(temp->coeffs+(n>>1)+i-1, poly+i, poly+(n>>1)+i-1, precision);
        arb_mul(temp->coeffs+(n>>1)+i-1, temp->coeffs+(n>>1)+i-1, inv_racine_2, precision);
    }
    
    for(slong i=0; i<n; i+=1){
        arb_set(poly+i, temp->coeffs+i);
    }
    arb_poly_clear(temp);
    return;
}

/*
    IMPLEMENTATION DE LA DCT 2
    ET DE LA DCT 4
*/

void dct2(arb_ptr, slong);
void dct4(arb_ptr, slong);

void dct2(arb_ptr poly, slong n) {
    if(n==2) {
        arb_t coef; arb_init(coef);
        arb_set(coef, poly+1);
        arb_set(poly+1, poly);
        arb_add(poly, poly, coef, precision);
        arb_sub(poly+1, poly+1, coef, precision);
        arb_clear(coef);
        return;
    }
    
    T0(poly, n);
    dct2(poly, n>>1);
    dct4(poly+(n>>1), n>>1);
    P(poly, n);
    return;
}

void dct4(arb_ptr poly, slong n){
    if(n==2){
        arb_t temp_1, temp_2;
        arb_init(temp_1); arb_init(temp_2);
        
        arb_mul(temp_1, coef_dct4_1, poly, precision);
        arb_mul(temp_2, coef_dct4_3, poly+1, precision);
        arb_add(temp_1, temp_1, temp_2, precision);
        
        arb_mul(temp_2, coef_dct4_3, poly, precision);
        arb_set(poly, temp_1);
        arb_mul(temp_1, coef_dct4_9, poly+1, precision);
        arb_add(poly+1, temp_1, temp_2, precision);

        arb_mul(poly, poly, racine_2, precision);
        arb_mul(poly+1, poly+1, racine_2, precision);
        
        arb_clear(temp_1); arb_clear(temp_2);
        return;
    }
    
    T1(poly, n);
    mul_racine_2(poly, n);
    dct2(poly, n>>1);
    dct2(poly+(n>>1), n>>1);
    A(poly, n);
    P(poly, n);
    return;
}

/*
    DECOMPOSITION DU POLYNOME
    POUR L'EVALUATION RAPIDE
    EN LES NOEUDS DE CHEBYSHEV
*/

void P_inv(arb_ptr poly, slong n){
    arb_poly_t temp; arb_poly_init(temp);
    arb_poly_fit_length(temp, n);
    _arb_poly_set_length(temp, n);

    for(slong i=0; i<(n>>1); i+=1){
        arb_set(temp->coeffs+i, poly+(i<<1));
        arb_set(temp->coeffs+(n>>1)+i, poly+(i<<1)+1);
    }
    
    for(slong i=0; i<n; i+=1){
        arb_set(poly+i, temp->coeffs+i);
    }
    return;
}

void decompose(arb_ptr poly, slong n){
    slong i = n>>1;
    P_inv(poly, n);
    while(i>1) {
        for(slong j=0; j<n; j+=i){
            _arb_poly_compose(poly+j, poly+j, i, transfo->coeffs, 2, precision);
            P_inv(poly+j, i);
        } i = i>>1;
    }
    return;
}

/*
    EVALUATION EN LES
    NOEUDS DE TCHEBYCHEV
*/

void eval(arb_ptr poly, slong n){
    for(slong i = 2; i <= n; i=i<<1){
        arb_poly_t nc; arb_poly_init(nc);
        arb_poly_fit_length(nc, i);
        _arb_poly_set_length(nc, i);

        for(slong k=0; k<i; k+=1){
            arb_set_si(nc->coeffs+k, (k<<1)+1);
            arb_div_si(nc->coeffs+k, nc->coeffs+k, -(i<<1), precision);
            arb_cos_pi(nc->coeffs+k, nc->coeffs+k, precision);
        }
        
        arb_t v1, v2; arb_init(v1); arb_init(v2);
        arb_t v3, v4; arb_init(v3); arb_init(v4);

        if(i==2){
            for(slong j=0; j<n; j+=i){
                arb_mul(v1, poly+j+1, nc->coeffs, precision);
                arb_add(v1, v1, poly+j, precision);
                arb_mul(v2, poly+j+1, nc->coeffs+1, precision);
                arb_add(v2, v2, poly+j, precision);   

                arb_set(poly+j, v1);
                arb_set(poly+j+1, v2);     
            }
        }

        else{
            for(slong j=0; j<n; j+=i){
                for(slong k=0; k<(i>>2); k+=1){
                    arb_mul(v1, poly+j+(i>>1)+k, nc->coeffs+k, precision);
                    arb_add(v1, v1, poly+j+k, precision);
                    arb_mul(v2, poly+j+i-k-1, nc->coeffs+(i>>1)-k-1, precision);
                    arb_add(v2, v2, poly+j+(i>>1)-k-1, precision);

                    arb_mul(v3, poly+j+i-k-1, nc->coeffs+(i>>1)+k, precision);
                    arb_add(v3, v3, poly+j+(i>>1)-k-1, precision);
                    arb_mul(v4, poly+j+(i>>1)+k, nc->coeffs+i-k-1, precision);
                    arb_add(v4, v4, poly+j+k, precision);
                
                    arb_set(poly+j+k, v1);
                    arb_set(poly+j+(i>>1)-k-1, v2);
                    arb_set(poly+j+(i>>1)+k, v3);
                    arb_set(poly+j+i-k-1, v4);
                }
            }
        }

        arb_poly_clear(nc);
        arb_clear(v1); arb_clear(v2);
        arb_clear(v3); arb_clear(v4);
    }
    return;
}

/*
    CONVERSION
    DE LA BASE CANONIQUE
    VERS LA BASE
    DE TCHEBYCHEV
*/

void conversion(arb_ptr poly, slong n){
    // n = taille(poly) est supposé une puissance de 2
    decompose(poly, n);
    eval(poly, n);

    dct2(poly, n);
    arb_div_si(poly, poly, n, precision);
    for(slong i=1; i<n; i+=1) {
        arb_div_si(poly+i, poly+i, n, precision);
        arb_mul(poly+i, poly+i, racine_2, precision);
    }
    return;
}


static void conversion_cheb(char **args, npy_intp *dimensions,
                      npy_intp* steps, void* data)
{
    init();
    
    npy_intp i;
    npy_intp is=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *in=args[0];
    char *op=args[1];
    
    slong taille = n;
    arb_poly_t x; arb_poly_init(x);
    arb_poly_fit_length(x, taille);
    _arb_poly_set_length(x, taille);
    slong k;
    arb_t t; arb_init(t);

    for(i=0; i<n; i+=1) {
        double *y = (double*) (in+i*is);

        k=i;
        arb_set_d(x->coeffs+k, y[0]);
        arb_set_d(t, y[1]);
        arb_add_error(x->coeffs+k, t);
    }

    conversion(x->coeffs, taille);
    
    for(i=0; i<n; i+=1) {
        double *z = (double*) (op+i*os);

        k=i;
        z[0] = arf_get_d(arb_midref(x->coeffs+i), ARF_RND_CEIL);
        arb_get_rad_arb(t, x->coeffs+i);
        z[1] = arf_get_d(arb_midref(t), ARF_RND_CEIL);
    }
    
    arb_clear(t);
    arb_poly_clear(x);

    finit(); return;
}

/*
    https://docs.scipy.org/doc/numpy-1.15.1/user/c-info.ufunc-tutorial.html
*/
static PyMethodDef UfuncIntervalsMethods[] = {
    {NULL, NULL, 0, NULL}
};

#if defined(NPY_PY3K)
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "converse",
    NULL,
    -1,
    UfuncIntervalsMethods,
    NULL,
    NULL,
    NULL,
    NULL
};
#endif

#if defined(NPY_PY3K)
    PyMODINIT_FUNC PyInit_converse(void)
#else
    PyMODINIT_FUNC initconverse(void)
#endif

{
    PyObject *m, *conversion, *d;
    PyObject *dtype_dict;
    PyArray_Descr *dtype;
    PyArray_Descr *dtypes[2];
    
#if defined(NPY_PY3K)
    m = PyModule_Create(&moduledef);
#else
    m = Py_InitModule("converse", UfuncIntervalsMethods);
#endif
    
    if (m == NULL) {
#if defined(NPY_PY3K)
        return NULL;
#else
        return;
#endif
    }
    
    import_array();
    import_umath();
    
    dtype_dict = Py_BuildValue("[(s, s), (s, s)]",
                               "center", "float64", "radius", "float64");
    PyArray_DescrConverter(dtype_dict, &dtype);
    Py_DECREF(dtype_dict);
    
    dtypes[0] = dtype; dtypes[1] = dtype;
    d = PyModule_GetDict(m);
    
    conversion = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                            PyUFunc_None, "conversion",
        "Convertit un polynôme en base canonique, donné par le tableau de ses coefficients,\n en ses coefficients dans la base des polynômes de Tchebychev.\n La taille du polynôme (taille >= degré+1) doit être une puissance de 2 supérieure à 2", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) conversion,
                                 dtype,
                                 &conversion_cheb,
                                 dtypes,
                                 NULL);
    PyDict_SetItemString(d, "conversion", conversion);
    Py_DECREF(conversion);
    
#if defined(NPY_PY3K)
    return m;
#endif
}

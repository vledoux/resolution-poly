#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include "Python.h"
#include "math.h"
#include "float.h"
#include "fenv.h"
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

const double MOINS_INF = - FLT_MAX;

static void abs_cumul(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = fabs(x[0])+x[1];
        }
    }
    for (i=n-2; i>=0; i--) {
        double *z_i = (double*) (op+i*os);
        double *z_i1 = (double*) (op+(i+1)*os);
        z_i[0] += z_i1[0];
    }
    fesetround(rounding_mode);
}

static void abs_cumul_u(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = (i+1)*(fabs(x[0])+x[1]);
        }
    }
    for (i=n-2; i>=0; i--) {
        double *z_i = (double*) (op+i*os);
        double *z_i1 = (double*) (op+(i+1)*os);
        z_i[0] += z_i1[0];
    }
    fesetround(rounding_mode);
}

static void err_proj(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_DOWNWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = sqrt(1-x[0]*x[0]);
        }
    }
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *z = (double*) (op+i*os);
            z[0] = 1./z[0];
        }
    }
    fesetround(rounding_mode);
}

static void calcul_err_proj(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1], is3=steps[2];
    npy_intp os=steps[3];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1], *i3=args[2];
    char *op=args[3];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *d = (double*) (i1+i*is1);
            double *p = (double*) (i2+i*is2);
            double *u = (double*) (i3+i*is3);
            double *z = (double*) (op+i*os);
            z[0] = fmin(d[0]*p[0], d[0]+u[0]);
        }
    }
    fesetround(rounding_mode);
}

static void rotation(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = x[2]*x[0];
            double err=z[0]-nextafter(z[0], MOINS_INF);
            z[0] = 2*z[0]+x[1];
            z[1] = -x[0]; z[2] = x[2];
            z[3] = x[3]+2*err+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void addition(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    //attention, on suppose y[1]=0
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1), *y = (double*) (i2+i*is2);
            double *z = (double*) (op+i*os);
            z[0] = x[0]+y[0]; z[1] = x[1]; z[2] = x[2];
            z[3] = x[3]+y[3]+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void split_min(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = x[0]-x[1]/2.;
            z[1] = x[1]/2.+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void split_max(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = x[0]+x[1]/2.;
            z[1] = x[1]/2.+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void tri_solutions(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *y1 = (double*) (i1+i*is1);
            double *y2 = (double*) (i2+i*is2);
            int *z = (int*) (op+i*os);
            if (fabs(y1[0]) < y1[1] || fabs(y2[0]) < y2[1]) {
                z[0] = 2;
            } else {
                if (y1[0]*y2[0] <= 0) {
                    z[0] = 1;
                } else {
                    z[0] = 0;
                }
            }
        }
    }
    fesetround(rounding_mode);
}

/*
    https://docs.scipy.org/doc/numpy-1.15.1/user/c-info.ufunc-tutorial.html
*/
static PyMethodDef UfuncIntervalsMethods[] = {
    {NULL, NULL, 0, NULL}
};

#if defined(NPY_PY3K)
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "operations_bis",
    NULL,
    -1,
    UfuncIntervalsMethods,
    NULL,
    NULL,
    NULL,
    NULL
};
#endif

PyUFuncGenericFunction funcs_proj[1] = {&err_proj};
PyUFuncGenericFunction funcs_proj_err[1] = {&calcul_err_proj};
static char types_proj[2] = {NPY_DOUBLE, NPY_DOUBLE};
static char types_proj_err[4] = {NPY_DOUBLE, NPY_DOUBLE, NPY_DOUBLE, NPY_DOUBLE};
static void *data[1] = {NULL};

#if defined(NPY_PY3K)
    PyMODINIT_FUNC PyInit_operations_bis(void)
#else
    PyMODINIT_FUNC initoperations_bis(void)
#endif

{
    PyObject *m, *err_cumul, *err_cumul_u, *rotat, *add_im, *d;
    PyObject *split_bas, *split_haut, *tri;
    PyObject *dtype_dict, *dtype_dict_im;
    PyArray_Descr *dtype, *dtype_im;
    PyArray_Descr *dtypes_2[2];
    PyArray_Descr *dtypes_2_bis[2];
    PyArray_Descr *dtypes_3[3];
    PyArray_Descr *dtypes_2_im[2];
    PyArray_Descr *dtypes_3_im[3];
    
#if defined(NPY_PY3K)
    m = PyModule_Create(&moduledef);
#else
    m = Py_InitModule("operations_bis", UfuncIntervalsMethods);
#endif
    
    if (m == NULL) {
#if defined(NPY_PY3K)
        return NULL;
#else
        return;
#endif
    }
    
    import_array();
    import_umath();
    
    dtype_dict = Py_BuildValue("[(s, s), (s, s)]",
                               "center", "float64", "radius", "float64");
    PyArray_DescrConverter(dtype_dict, &dtype);
    Py_DECREF(dtype_dict);
    dtype_dict_im = Py_BuildValue("[(s, s), (s, s), (s, s), (s, s)]",
                               "real", "float64", "imaginary", "float64",
                               "vect", "float64", "err_radius", "float64");
    PyArray_DescrConverter(dtype_dict_im, &dtype_im);
    Py_DECREF(dtype_dict_im);
    
    dtypes_2[0] = dtype; dtypes_2[1] = dtype;
    dtypes_2_bis[0] = dtype; dtypes_2_bis[1] = PyArray_DescrFromType(NPY_DOUBLE);
    dtypes_3[0] = dtype; dtypes_3[1] = dtype;
    dtypes_3[2] = PyArray_DescrFromType(NPY_INT);
    dtypes_2_im[0] = dtype_im; dtypes_2_im[1] = dtype_im;
    dtypes_3_im[0] = dtype_im; dtypes_3_im[1] = dtype_im; dtypes_3_im[2] = dtype_im;
    d = PyModule_GetDict(m);
    
    err_cumul = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                  PyUFunc_None, "err_cumul",
                                  "[t_0, ..., t_(n-1), t_n] -> [|t_0| +...+ |t_n|, ..., |t_(n-1)| + |t_n|, |t_n|]", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) err_cumul,
                                 dtype,
                                 &abs_cumul,
                                 dtypes_2_bis,
                                 NULL);
    PyDict_SetItemString(d, "err_cumul", err_cumul);
    Py_DECREF(err_cumul);

    err_cumul_u = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                  PyUFunc_None, "err_cumul_u",
                                  "[t_0, ..., t_(n-1), t_n] -> [|t_0| +...+ n*|t_n|, ..., (n-1)*|t_(n-1)| + n*|t_n|, n*|t_n|]", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) err_cumul_u,
                                 dtype,
                                 &abs_cumul_u,
                                 dtypes_2_bis,
                                 NULL);
    PyDict_SetItemString(d, "err_cumul_u", err_cumul_u);
    Py_DECREF(err_cumul_u);
    
    split_bas = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                        PyUFunc_None, "split_bas",
                                        "[a, b] -> [a, (a+b)/2]", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) split_bas,
                                 dtype,
                                 &split_min,
                                 dtypes_2,
                                 NULL);
    PyDict_SetItemString(d, "split_bas", split_bas);
    Py_DECREF(split_bas);
    
    split_haut = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                        PyUFunc_None, "split_haut",
                                        "[a, b] -> [(a+b)/2, b]", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) split_haut,
                                 dtype,
                                 &split_max,
                                 dtypes_3,
                                 NULL);
    PyDict_SetItemString(d, "split_haut", split_haut);
    Py_DECREF(split_haut);
    
    tri = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 2, 1,
                                         PyUFunc_None, "tri",
                                         "pour un ensemble d'intervalles sur lequel P est monotone, renvoie \n0 si P ne s'annule pas sur I \n1 si P a une racine sur I\n 2 si la fonction ne conclue pas", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) tri,
                                 dtype,
                                 &tri_solutions,
                                 dtypes_3,
                                 NULL);
    PyDict_SetItemString(d, "tri", tri);
    Py_DECREF(tri);
    
    PyObject* proj;
    proj = PyUFunc_FromFuncAndData(funcs_proj, data, types_proj, 1, 1, 1,
                                       PyUFunc_None, "proj",
                                       "1/sqrt(1-a^2), utile pour les calculs d'erreurs", 0);
    PyDict_SetItemString(d, "proj", proj);
    Py_DECREF(proj);
    
    PyObject* proj_err;
    proj_err = PyUFunc_FromFuncAndData(funcs_proj_err, data, types_proj_err, 1, 3, 1,
                                   PyUFunc_None, "proj_err",
                                   "calcul de l'erreur lors de la projection de l'intervalle sur les réels", 0);
    PyDict_SetItemString(d, "proj_err", proj_err);
    Py_DECREF(proj_err);
    
    rotat = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                        PyUFunc_None, "rotat",
            "fait une rotation de conjugué(g) à un intervalle de centre a + bg, a et b réels, g de module 1", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) rotat,
                                 dtype_im,
                                 &rotation,
                                 dtypes_2_im,
                                 NULL);
    PyDict_SetItemString(d, "rotat", rotat);
    Py_DECREF(rotat);
    
    add_im = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 2, 1,
                                    PyUFunc_None, "add_im",
            "additionne un intervalle complexe et un intervalle réel", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) add_im,
                                 dtype_im,
                                 &addition,
                                 dtypes_3_im,
                                 NULL);
    PyDict_SetItemString(d, "add_im", add_im);
    Py_DECREF(add_im);
    
#if defined(NPY_PY3K)
    return m;
#endif
}

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION

#include "Python.h"
#include "math.h"
#include "float.h"
#include "fenv.h"
#include "numpy/ndarraytypes.h"
#include "numpy/ufuncobject.h"
#include "numpy/npy_3kcompat.h"

/*
    Ce fichier définit les opérations basiques sur 
    des intervalles de la forme (centre, rayon) ;
    - arithmétique : 
        addition, soustraction, multiplication et négation ;
    - extrémités de l'intervalle : 
        borne_inf et borne_sup ;
    - d'une chaine de caracteres vers un intervalle : 
        strtod_centre et strtod_radius.
*/

const double MOINS_INF = - FLT_MAX;

static void addition(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1), *y = (double*) (i2+i*is2);
            double *z = (double*) (op+i*os);
            z[0] = x[0]+y[0];
            z[1] = x[1]+y[1]+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void soustraction(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1), *y = (double*) (i2+i*is2);
            double *z = (double*) (op+i*os);
            z[0] = x[0]-y[0];
            z[1] = x[1]+y[1]+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void multiplication(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1), *y = (double*) (i2+i*is2);
            double *z = (double*) (op+i*os);
            z[0] = x[0]*y[0];
            z[1] = x[1]*fabs(y[0])+y[1]*fabs(x[0])
                +x[1]*y[1]+(z[0]-nextafter(z[0], MOINS_INF));
        }
    }
    fesetround(rounding_mode);
}

static void negation(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    #pragma omp parallel
    {
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = -x[0];
            z[1] = x[1];
        }
    }
}

static void borne_inf(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = x[0]-x[1];
            z[1] = z[0]-nextafter(z[0], MOINS_INF);
        }
    }
}

static void borne_sup(char **args, npy_intp *dimensions, npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i=0; i<n; i++) {
            double *x = (double*) (i1+i*is1);
            double *z = (double*) (op+i*os);
            z[0] = x[0]+x[1];
            z[1] = z[0]-nextafter(z[0], MOINS_INF);
        }
    }
}

static void strtod_center(char **args, npy_intp *dimensions,
                      npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0];
    npy_intp os=steps[1];
    npy_intp n=dimensions[0];
    
    char *i1=args[0];
    char *op=args[1];
    
    int rounding_mode = fegetround();
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i = 0; i < n; i++) {
            char *x = (char*)(i1+i*is1);
            double *res = (double*)(op+i*os);
            *res = strtod(x, NULL);
        }
    }
    fesetround(rounding_mode);
}

static void strtod_radius(char **args, npy_intp *dimensions,
                      npy_intp* steps, void* data)
{
    npy_intp i;
    npy_intp is1=steps[0], is2=steps[1];
    npy_intp os=steps[2];
    npy_intp n=dimensions[0];
    
    char *i1=args[0], *i2=args[1];
    char *op=args[2];
    
    int rounding_mode = fegetround();
    
    #pragma omp parallel
    {
        fesetround(FE_UPWARD);
        #pragma omp for
        for (i = 0; i < n; i++) {
            char *x = (char*)(i1+i*is1);
            char *y =(char*)(i2+i*is2);
            double *res = (double*)(op+i*os);
            double center = strtod(x, NULL);
            double error_center = fabs(center-nextafter(center, MOINS_INF));
            *res = strtod(y, NULL)+error_center;
        }
    }
    fesetround(rounding_mode);
}

/*
    https://docs.scipy.org/doc/numpy-1.15.1/user/c-info.ufunc-tutorial.html
*/
static PyMethodDef UfuncIntervalsMethods[] = {
    {NULL, NULL, 0, NULL}
};

#if defined(NPY_PY3K)
static struct PyModuleDef moduledef = {
    PyModuleDef_HEAD_INIT,
    "operations",
    NULL,
    -1,
    UfuncIntervalsMethods,
    NULL,
    NULL,
    NULL,
    NULL
};
#endif

PyUFuncGenericFunction funcs_strtod[1] = {&strtod_center};
PyUFuncGenericFunction funcs_strtod_bis[1] = {&strtod_radius};
static char types[2] = {NPY_INT8, NPY_DOUBLE};
static char types_bis[3] = {NPY_INT8, NPY_INT8, NPY_DOUBLE};
static void *data[1] = {NULL};


#if defined(NPY_PY3K)
    PyMODINIT_FUNC PyInit_operations(void)
#else
    PyMODINIT_FUNC initoperations(void)
#endif

{
    PyObject *m, *add, *sub, *mul, *neg, *d;
    PyObject *inf, *sup;
    PyObject *dtype_dict;
    PyArray_Descr *dtype;
    PyArray_Descr *dtypes_3[3];
    PyArray_Descr *dtypes_2[2];
    
#if defined(NPY_PY3K)
    m = PyModule_Create(&moduledef);
#else
    m = Py_InitModule("operations", UfuncIntervalsMethods);
#endif
    
    if (m == NULL) {
#if defined(NPY_PY3K)
        return NULL;
#else
        return;
#endif
    }
    
    import_array();
    import_umath();
    
    dtype_dict = Py_BuildValue("[(s, s), (s, s)]",
                               "center", "float64", "radius", "float64");
    PyArray_DescrConverter(dtype_dict, &dtype);
    Py_DECREF(dtype_dict);
    
    dtypes_3[0] = dtype; dtypes_3[1] = dtype; dtypes_3[2] = dtype;
    dtypes_2[0] = dtype; dtypes_2[1] = dtype;
    d = PyModule_GetDict(m);
    
    add = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 2, 1,
                                            PyUFunc_None, "add",
        "En entrée : deux tableaux d'intervalles, \nrenvoie leur somme terme à terme.", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) add,
                                 dtype,
                                 &addition,
                                 dtypes_3,
                                 NULL);
    PyDict_SetItemString(d, "add", add);
    Py_DECREF(add);
    
    sub = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 2, 1,
                                  PyUFunc_None, "sub",
        "En entrée : deux tableaux d'intervalles, \nrenvoie leur différence terme à terme.", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) sub,
                                 dtype,
                                 &soustraction,
                                 dtypes_3,
                                 NULL);
    PyDict_SetItemString(d, "sub", sub);
    Py_DECREF(sub);

    mul = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 2, 1,
                                  PyUFunc_None, "mul",
        "En entrée : deux tableaux d'intervalles, \nrenvoie leur produit terme à terme.", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) mul,
                                 dtype,
                                 &multiplication,
                                 dtypes_3,
                                 NULL);
    PyDict_SetItemString(d, "mul", mul);
    Py_DECREF(mul);
    
    neg = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                  PyUFunc_None, "neg",
        "En entrée : un tableau d'intervalles, \nrenvoie leur négation terme à terme.", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) neg,
                                 dtype,
                                 &negation,
                                 dtypes_2,
                                 NULL);
    PyDict_SetItemString(d, "neg", neg);
    Py_DECREF(neg);
    
    inf = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                  PyUFunc_None, "inf",
        "En entrée, un tableau d'intervalles (centre, rayon), \nrenvoie la borne inférieure de l'intervalle sous forme d'un intervalle \n(centre-rayon, inprécision(centre-rayon))", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) inf,
                                 dtype,
                                 &borne_inf,
                                 dtypes_2,
                                 NULL);
    PyDict_SetItemString(d, "inf", inf);
    Py_DECREF(inf);
    
    sup = PyUFunc_FromFuncAndData(NULL, NULL, NULL, 0, 1, 1,
                                  PyUFunc_None, "sup",
        "En entrée, un tableau d'intervalles (centre, rayon), \nrenvoie la borne supérieure de l'intervalle sous forme d'un intervalle \n(centre+rayon, inprécision(centre+rayon))", 0);
    PyUFunc_RegisterLoopForDescr((PyUFuncObject*) sup,
                                 dtype,
                                 &borne_sup,
                                 dtypes_2,
                                 NULL);
    PyDict_SetItemString(d, "sup", sup);
    Py_DECREF(sup);
    
    PyObject* strtod_c;
    strtod_c = PyUFunc_FromFuncAndData(funcs_strtod, data, types, 1, 1, 1,
                                       PyUFunc_None, "strtod_c",
        "En entrée, un tableau de chaînes des caractères qui représentent des nombres, \nrenvoie les nombres correspondants", 0);
    PyDict_SetItemString(d, "strtod_c", strtod_c);
    Py_DECREF(strtod_c);
    
    PyObject* strtod_r;
    strtod_r = PyUFunc_FromFuncAndData(funcs_strtod_bis, data, types_bis, 1, 2, 1,
                                       PyUFunc_None, "strtod_r",
        "En entrée, deux tableaux de chaînes de caractères \nqui représentent centres et rayons, \nrenvoie le nombre correspondant au rayon \nen prenant en compte l'imprécision sur les centre et rayon", 0);
    PyDict_SetItemString(d, "strtod_r", strtod_r);
    Py_DECREF(strtod_r);
    
#if defined(NPY_PY3K)
    return m;
#endif
}

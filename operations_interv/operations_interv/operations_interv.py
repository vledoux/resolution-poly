import numpy as np
from operations_interv import operations as op

class indarray(np.ndarray):
    def __array_ufunc__(self, ufunc, method, *inputs, **kwargs):
        # Converting input to type of nb_mul, nb_add, ...
        args = [e.view(type=np.ndarray) if isinstance(e, np.ndarray)
                    and e.dtype == np.dtype([('center', 'double'),('radius','double')])
                else sarray(e).view(type=np.ndarray)
                for e in inputs]
        output = kwargs.pop('out', None)
        if output:
            if isinstance(output[0], indarray):
                kwargs['out'] = (output[0].view(type=np.ndarray),)
            else:
                kwargs['out'] = output

        if ufunc == np.multiply:
            res = op.mul(*args, **kwargs)
        elif ufunc == np.add:
            res = op.add(*args, **kwargs)
        elif ufunc == np.subtract:
            res = op.sub(*args, **kwargs)
        elif ufunc == np.negative:
            res = op.neg(*args, **kwargs)
        else:
            res = ufunc(*args, **kwargs)

        # Converting output to default interval array
        if output is None:
            return np.asarray(res).view(type=type(self))
        else:
            return output[0]
            
    def __repr__(self):
        res = np.array(self)
        # increase precision to distinguish singleton intervals
        prec = max(18, np.get_printoptions()['precision'])
        return np.array2string(res, precision=prec)

    def __str__(self):
        interv = np.array(self)
        res = np.zeros(self.shape+(1,))
        res = np.repeat(res,2,-1)
        res[...,0] = interv['center']-interv['radius']
        res[...,1] = interv['center']+interv['radius']
        return np.array2string(res)

indarray.__module__ = 'operations_interv'

def numtostr(e):
    if isinstance(e, (str, bytes)):
        res = e
    elif isinstance(e, int):
        res = str(e)
    elif isinstance(e, float):
        res = e.hex()
    elif isinstance(e, np.inexact) and np.finfo(e).precision <= 15:
        res = float(e).hex()
    else:
        raise TypeError('element of type {0} cannot be represented'.format(type(e)))
    return res + '\0'

def iarray(in_array, dtype=None, copy=True, order='K', subok=False, ndmin=0):
    if dtype is not None and np.dtype(dtype).type != np.float64 :
        raise TypeError('only dtype np.float64 is supported')
    if isinstance(in_array, indarray):
        res = np.array(in_array, dtype=dtype, copy=copy, order=order, subok=subok, ndmin=ndmin)
    elif isinstance(in_array, np.ndarray)\
         and np.issubdtype(in_array.dtype, np.inexact)\
         and np.finfo(in_array.dtype).precision <= 15:
        res = np.array(in_array, dtype=np.float64, copy=copy, order=order, subok=subok, ndmin=ndmin)
        res = res.view(dtype=[('center','float64'),('radius','float64')]).squeeze(-1)
    else:
        in_str = np.vectorize(numtostr, otypes=['bytes'])(in_array)
        res = np.zeros_like(in_str, dtype=np.float64, order=order, subok=subok)
        if ndmin > res.ndim:
            res.shape = (1,)*(ndmin -res.ndim) + res.shape
        res = res.view(dtype=[('center','float64'),('radius','float64')]).squeeze(-1)
        in_start_char = in_str[...,None].view(dtype=np.int8)[...,0]
        res['center'] = op.strtod_c(in_start_char[...,0])
        res['radius'] = op.strtod_r(in_start_char[...,0], in_start_char[...,1])
    if subok==False:
        return res.view(type=indarray)
    else:
        return res

def sarray(*args, **kwargs):
    res = np.array(*args, **kwargs)
    res.shape = res.shape + (1,)
    res = np.repeat(res, 2, -1)
    res[...,1] *= 0
    return iarray(res, *(args[1:]), **kwargs)

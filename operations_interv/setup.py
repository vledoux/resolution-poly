def configuration(parent_package='', top_path=None):
    import numpy
    from numpy.distutils.misc_util import Configuration
    config = Configuration('operations_interv',
                           parent_package,
                           top_path)
    config.add_extension('operations', ['operations_interv/operations.c'],
                         extra_compile_args = ['-fopenmp', '-frounding-math'],
                         extra_link_args = ['-fopenmp'])
    config.add_extension('operations_bis', ['operations_interv/operations_bis.c'],
                        extra_compile_args = ['-fopenmp', '-frounding-math'],
                        extra_link_args = ['-fopenmp'])
    config.add_extension('converse', ['operations_interv/converse.c'],
                        extra_compile_args = ['-fopenmp', '-frounding-math', '-larb'],
                        extra_link_args = ['-fopenmp', '-larb'])
    return config

if __name__ == "__main__":
    from numpy.distutils.core import setup
    setup(configuration=configuration,
          packages = ['operations_interv'])

import numpy as np
import operations_interv as op
import utilitaire as util
import box as box
import split as s
import time as time

def extraire_racines(P, f, base_canonique=True, debug=0):
    '''
        Fonction principale.
        extraire_racines(P, f, debug=0)
        prend en arguments :
        - un polynome P donne par la liste de ces coefficients
        - une fonction f : tour -> epsilon
        qui permet de choisir avec quelle erreur d'approximation epsilon
        sera tronqué le polynome
        f = lambda x : len(P)/(2**x) peut être une solution par défaut
        - debug permet d'afficher différentes statistiques sur l'execution
        
        et renvoie
        - tableau d'intervalles contenant chacun exactement une racine,
        - tableau d'intervalles contenant chacun au plus une racine
        Toutes les racines sur [-1, 1] sont dans un intervalle
        dans un de ces deux tableaux.
    '''

    P = op.sarray(P)
    
    if(debug==1):
        t = time.time()

    # Conversion de P vers la base de Tchebychev
    ((P_cheb, P_err), (P_cheb_d, P_derr)) = util.conversion(P, base_canonique)
    
    if(debug==1):
        print("Temps conversion : ", time.time()-t)
        t=time.time()

    # Initialisations
    sol = (np.array([], dtype=[('center',np.float64),('radius',np.float64)])).view(type=op.indarray)

    # ne pas se contenter d'initialiser a_traiter avec [-1, 1],
    # sinon il y a des overflows dans l'évaluation par Clenshaw
    a_traiter = op.iarray([(0,1)])
    a_traiter = s.split(a_traiter)
    a_traiter = s.split(a_traiter)

    # Boucle principale
    tour = 1
    while np.size(a_traiter) != 0 :
        if(debug==2):
            print('tour :', tour, 'nb_segments :', len(a_traiter))
        
        # Choix du degré auquel le polynome sera tronqué
        epsilon, tour = f(tour), tour+1
        degre_err = util.calcul_deg_err(P_err, abs(epsilon))
        degre_err_derivee = util.calcul_deg_err(P_derr, abs(epsilon))
        
        if(debug==2):
            print('degre approx P :\t', degre_err)
            print('degre approx P\' :\t', degre_err_derivee)
        
        # evalue P sur chaque intervalle, et
        # elimine ceux dont l'image ne contient pas 0
        image_P = box.box_inter_0(P_cheb[:degre_err], a_traiter, True, abs(epsilon))
        a_traiter = a_traiter[image_P]

        # evalue P' sur chaque intervalle
        # si son image ne contient pas 0, P est monotone
        image_derivee_P = box.box_inter_0(P_cheb_d[:degre_err_derivee], a_traiter, base_canonique, abs(epsilon))
        sol_incr = a_traiter[np.logical_not(image_derivee_P)]
        sol = np.concatenate((sol, sol_incr)).view(type=op.indarray)

        # les intervalles restants sont subdivises
        a_traiter = a_traiter[image_derivee_P]        
        a_traiter = s.split(a_traiter)
    
    if(debug==1):
        print("Temps resolution : ", time.time()-t)
        t=time.time()
    
    # pour chaque intervalle où P est monotone
    # evalue P a chaque extremite de l'intervalle
    # pour determiner s'il contient une racine
    # filtre = 0, pas de racines
    # 1, une seule racine, 2, impossible de conclure
    filtre=util.filtre_sol(P, sol, base_canonique)

    if(debug==1):
        print("Temps vérification : ", time.time()-t)
    
    return sol[filtre==1], sol[filtre==2]

def extraire_racines_horner(P):
    P = op.sarray(P)
    degre=len(P)
    facteur = np.fromfunction(lambda i:(i+1), (degre-1,),dtype=int)
    P_derivee = op.sarray(facteur)*(np.roll(P,-1)[:degre-1:])

    sol = (np.array([], dtype=[('center',np.float64),('radius',np.float64)])).view(type=op.indarray)
    a_traiter = op.iarray([(0,1)])
    a_traiter = s.split(a_traiter)
    a_traiter = s.split(a_traiter)

    def eval_horner(f, interv):
        y = op.sarray(np.zeros(len(interv)))
        for i in f[::-1]:
            y = y*interv + np.array([i]).view(op.indarray)
        return y
    def inter_0(f, interv):
        y = np.array(eval_horner(f, interv))
        def contient_zero(c, r) :
            return (abs(c) <= r)
        return contient_zero(y['center'], y['radius'])

    while np.size(a_traiter) != 0 :
        image_P = inter_0(P, a_traiter)
        a_traiter = a_traiter[image_P]

        image_derivee_P = inter_0(P_derivee, a_traiter)
        sol_incr = a_traiter[np.logical_not(image_derivee_P)]
        sol = np.concatenate((sol, sol_incr)).view(type=op.indarray)

        a_traiter = a_traiter[image_derivee_P]        
        a_traiter = s.split(a_traiter)
    
    filtre=util.filtre_sol(P, sol)
    return sol[filtre==1], sol[filtre==2]

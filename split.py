import numpy as np
import operations_interv as op

def mid_bas(interv):
    # attention, renvoie un array numpy
    return op.split_bas(interv)

def mid_haut(interv):
    # attention, renvoie un array numpy
    return op.split_haut(interv)

def split(interv):
    return np.concatenate((mid_bas(interv), mid_haut(interv))).view(type=op.indarray)

import numpy as np
import time as time
import resol as resol

def affiche(r1, r2, s1, s2):
    def affiche_q(x):
        if(x[0]>0):
            print('(', 1/(x[0]+x[1]), ',', 1/(x[0]-x[1]), ')')
        else:
            print('(', 1/(x[0]-x[1]), ',', 1/(x[0]+x[1]), ')')
        return

    print("Racines :")
    s1 = np.array(s1)
    print(r1)
    for x in s1 :
        affiche_q(x)
    print("Non conclu :")
    s2 = np.array(s2)
    print(r2)
    for x in s2 :
        affiche_q(x)
    return

def resolution(P, f, base_canonique=True, debug=0):
    r1, r2 = resol.extraire_racines(P, f, base_canonique, debug)
    Q=list(P)
    Q.reverse()
    s1, s2 = resol.extraire_racines(Q, f, base_canonique, debug)
    return (r1, r2, s1, s2)
    

def test_err_approx(n):
    f0 = lambda x : np.sqrt(n)/(2**x)
    f1 = lambda x : 1/(2*x)
    f2 = lambda x : n/(2**x)
    f3 = lambda x : 1/(2**x)
    f4 = lambda x : 0

    P = np.random.randn(n)

    t=time.time()
    print("epsilon = sqrt(len(P))/(2**tour) :")
    _ = resolution(P, f0)
    print(time.time()-t, " s")

    #t=time.time()
    #print("epsilon = 1/(2*tour) :")
    #_ = resolution(P, f1)
    #print(time.time()-t, " s")

    t=time.time()
    print("epsilon = len(P)/(2**tour) :")
    _ = resolution(P, f2)
    print(time.time()-t, " s")

    #t=time.time()
    #print("epsilon = 1/(2**tour) :")
    #_ = resolution(P, f3)
    #print(time.time()-t, " s")

    #t=time.time()
    #print("epsilon = 0 :")
    #_ = resolution(P, f4)
    #print(time.time()-t, " s")
    return

def test_perf(n, x):
    f = lambda y : n/(2**y)
    temps_total_1, temps_total_2 = 0, 0

    for i in range(x):
        P = np.random.randn(n)
        t = time.time()
        _ = resolution(P, f, True, 1)
        temps_total_1 += time.time() - t

    for i in range(x):
        P = np.random.randn(n)
        for j in range(n):
            P[j] /= np.sqrt(j+1)
        t = time.time()
        _ = resolution(P, f, True, 1)
        temps_total_2 += time.time() - t

    print("Temps moyen c_i = N(0,1) : ", temps_total_1/x)
    print("Temps moyen c_i = N(0,1)/sqrt(i) : ", temps_total_2/x)    

def test_perf_tcheb(n, x):
    f = lambda y : n/(2**y)
    temps_total = 0

    for i in range(x):
        P = np.random.randn(n)
        t=time.time()
        _ = resolution(P, f, False, 1)
        temps_total += time.time() -t
    print("Temps moyen série Tchebychev : ", temps_total/x)

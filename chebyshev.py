import numpy as np
import operations_interv as op

def to_chebyshev(P):
    '''transformation d'un polynome de la base canonique vers la base de Tchebychev'''
    n = len(P)
    if n <= 1 :
        return P
    
    # Pour utiliser un autre algorithme,
    # qui passe par une évaluation en les noeuds de Tchebychev et une DCT,
    # plus lent car manipule des polynômes plus grands (de taille 2^k)
    '''  
    k=2
    while(k < n):
        k*=2
    
    if(k==n):
        return op.conversion(P)
    
    c = np.array(P)
    a = np.array(op.iarray(np.zeros((k-n, 2))))
    c = np.concatenate((c, a), axis=0)
    c = c.view(dtype=[('center',np.float64),('radius',np.float64)])
    return op.conversion(c)'''

    x_k = op.iarray(np.zeros((n,2)))
    x_k[0] = (1,0)
    cheb = op.iarray(np.zeros((n,2)))
    for i in range(n) :
        # x_k contient les coefficients a_j tels que
        # x^k = a_0 + a_1*T_1(x) + ... + a_(n-1)*T_(n-1)(x)
        cheb = cheb+P[i:(i+1):]*x_k
        # calcul de x_(k+1)
        pb, x_k[0] = np.copy(x_k[0:1:]), (0,0) #pb de condition limite
        x_k = 0.5*(np.roll(x_k, 1) + np.roll(x_k, -1))
        x_k[1] = (x_k[1:2:]+pb)[0]
    return cheb
